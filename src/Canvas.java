import objectdata.Point2D;
import objectdata.Polygon2D;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import rasterdata.Presentable;
import rasterdata.RasterImage;
import rasterdata.RasterImageBI;
import rasterops.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * trida pro kresleni na platno: zobrazeni pixelu
 * 
 * @author PGRF FIM UHK
 * @version 2020
 */

public class Canvas {

	private JFrame frame;
	private JPanel panel;
	private final @NotNull RasterImage<Integer> img;
	private final @NotNull Presentable<Graphics> presenter;
	private final @NotNull Liner<Integer> liner;
	private final @NotNull Liner<Integer> dottedLiner;
	private final @NotNull Polygon2D polygon2D;
	private final @NotNull Polygoner2D<Integer> polygoner2D;
	private int c1, r1, c2, r2;
//	private final @NotNull Polygon2D polygonTest;
	private int mode=1;
	private int x;
	private int y;

	public Canvas(int width, int height) {

		frame = new JFrame();

		frame.setLayout(new BorderLayout());
		frame.setTitle("UHK FIM PGRF : " + this.getClass().getName());
		frame.setResizable(false);
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		final @NotNull RasterImageBI auxRasterImageBI = new RasterImageBI(width, height);
		img = auxRasterImageBI;
		presenter = auxRasterImageBI;
		polygon2D = new Polygon2D();
		polygoner2D = new Polygoner2D<>();
		liner = new TrivialLiner<>();
		dottedLiner = new DottedLiner<>(1, 5);

		panel = new JPanel() {
			private static final long serialVersionUID = 1L;

			@Override
			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				present(g);
			}
		};

		panel.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				clear();
				c2 = e.getX();
				r2 = e.getY();


				if(mode==1){
					liner.drawLine(img,c1, r1, e.getX(), e.getY(), 0xff00ff);
				}

				if(mode==2){
					dottedLiner.drawLine(img, c1, r1, e.getX(), e.getY(), 0xff00ff);
				}

				if(mode==3){
					liner.drawLine(img, Polygon2D.getPoint(0).getC1(), Polygon2D.getPoint(0).getR1(), e.getX(), e.getY(), 0xff00ff);
					liner.drawLine(img, x, y, e.getX(), e.getY(), 0xff00ff);
					polygoner2D.drawPolygon(img, 0xffff00, liner, polygon2D);
				}

				present();
			}
		});

		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				c1 = e.getX();
				r1 = e.getY();
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				c2 = e.getX();
				r2 = e.getY();
				if(mode==2){
					draw(() -> dottedLiner.drawLine(img, c1, r1, c2, r2, 0xfff000));
				}
				else if(mode==1){
					draw(() -> liner.drawLine(img, c1, r1, c2, r2, 0xfff000));
				}
				else if(mode==3){
					mousePressed(e);
					draw(() -> {
						x=e.getX();
						y=e.getY();
						polygon2D.addPoint(new Point2D(e.getX(), e.getY()));
						polygoner2D.drawPolygon(img, 0xffff00, liner, polygon2D);
					});
				}
//				clear();
//				liner.drawLine(img, c1, r1, c2, r2, 0xff0000);
//				present();
			}
		});

		panel.setPreferredSize(new Dimension(width, height));

		frame.add(panel, BorderLayout.CENTER);
		frame.pack();
		frame.setVisible(true);
		/*polygonTest = new Polygon2D();
		polygonTest.addPoint(new Point2D(20, 100));
		polygonTest.addPoint(new Point2D(100, 60));
		polygonTest.addPoint(new Point2D(200, 100));*/

		frame.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {

			}

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_C) {
					System.out.println("C was pressed!-Clear the board");
					clear();
				} else if (e.getKeyCode() == KeyEvent.VK_K) {
					System.out.println("K was pressed-TrivialLiner");
					mode=1;
				} else if (e.getKeyCode() == KeyEvent.VK_B) {
					System.out.println("B was pressed-DottedLiner");
					mode=2;
				}else if (e.getKeyCode() == KeyEvent.VK_V) {
					System.out.println("V was pressed-Polygon");
					mode=3;
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {

			}
		});

	}

	public void draw(final @NotNull Runnable r) {
		clear();
		r.run();
		present();
	}

	public void clear() {
		img.clear(0x2f2f2f);
	}

	public void present(final @NotNull Graphics graphics) {
		presenter.present(graphics);
	}

	public void present() {
		final @Nullable Graphics g = panel.getGraphics();
		if (g != null) {
			presenter.present(g);
		}
	}

	public void start() {
		clear();
//		liner.drawLine(img, 10, 10, 20, 10, 0xffff00);
		present();
	}


	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> new Canvas(800, 600).start());
	}

}