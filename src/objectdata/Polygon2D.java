package objectdata;

import java.util.ArrayList;
import java.util.List;

public class Polygon2D {

    private static final List<Point2D> points = new ArrayList<>();

    public Polygon2D() {
    }

    public void addPoint(Point2D point){
        points.add(point);
    }

    public static Point2D getPoint(int index){
        return points.get(index);
    }

    public List<Point2D> getPoints(){
        return points;
    }
}
